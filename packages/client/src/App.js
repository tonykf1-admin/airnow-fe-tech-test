import { useState, useEffect } from 'react';
import moment from 'moment';
import { useMutation } from '@apollo/client';
import { InstalledSdks as installedSdksQuery } from './query/installedSdks';
import { UninstalledSdks as uninstalledSdksQuery } from './query/uninstalledSdks';
import ButtonGroup from './components/buttonGroup';
import { INSTALLED_SDK, UNINSTALLED_SDK } from './constants';
import './App.css';

function App() {

  const handleQueryResult = (d) => {
    setData(d[section]);

    const lastSeenDates = d[section].map(data => moment(data.lastSeenDate));
    setLastModifyDate(moment.max(lastSeenDates).format('MMM D, YYYY'));

    const groupedList = {};
    d[section].forEach(({ name, categories, lastSeenDate = [] }) => {
      categories.forEach(category => {
        groupedList[category] = groupedList[category] || [];
        groupedList[category].push({
          name,
          date: moment(lastSeenDate).fromNow()
        });
      });
    });
    setList(groupedList);
  };

  const [section, setSection] = useState(INSTALLED_SDK);
  const [data, setData] = useState([]);
  const [lastModifyDate, setLastModifyDate] = useState();
  const [list, setList] = useState({});
  const [getInstalledSdks] = useMutation(installedSdksQuery, { onCompleted: handleQueryResult });
  const [getUninstalledSdks] = useMutation(uninstalledSdksQuery, { onCompleted: handleQueryResult });

  const sections = [
    {
      label: 'Installed',
      value: INSTALLED_SDK
    },
    {
      label: 'Uninstalled',
      value: UNINSTALLED_SDK
    }
  ];

  const switchSection = (value) => {
    setSection(value);
  }

  useEffect(() => {
    section === INSTALLED_SDK
      ? getInstalledSdks()
      : getUninstalledSdks()
  }, [section, getInstalledSdks, getUninstalledSdks]);

  return (
    <div className="App">
      <header className="App-header">
        <ButtonGroup
          items={sections}
          onClick={switchSection}
        />
      </header>
      <main className="App-body">
        <div className="title">
          <div className="column">
            <h1>
              {section === INSTALLED_SDK ? 'Installed SDK\'s' : 'Uninstalled SDK\'s'}
              {lastModifyDate && <span className="date">{`Latest Update: ${lastModifyDate}`}</span>}
            </h1>
          </div>
          <div className="column">
            <span className="count">
              {data.length}
            </span>
          </div>
        </div>
        <div className="list">
          {
            list && Object.keys(list).sort().map(key => (
              <div
                key={key}
                className="section"
              >
                <h4>{key}</h4>
                <ul>
                  {
                    list[key] && list[key].map(item => (
                      <li key={item.name}>
                        <span>{item.name}</span>
                        <span>{item.date}</span>
                      </li>
                    ))
                  }
                </ul>
              </div>
            ))
          }
        </div>
      </main>
    </div>
  );
}

export default App;
