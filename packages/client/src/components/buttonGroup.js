import { useState } from 'react';
import './buttonGroup.css';

export default function ButtonGroup({
	items = [],
	onClick,
}) {
	const [active, setActive] = useState(items[0] && items[0].value);

	const handleClick = ({
		value
	}) => {
		onClick && onClick(value);
		setActive(value);
	}

	return (
		<div className="buttonGroup">
			{
				items.map(item => (
					<button
						key={item.value}
						className={active === item.value ? 'active' : null}
						onClick={() => { handleClick(item) }}
					>
						{item.label}
					</button>
				))
			}
		</div>
	);
}
