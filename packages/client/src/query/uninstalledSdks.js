import { gql } from "@apollo/client";

export const UninstalledSdks = gql`
  mutation UninstalledSdks {
    uninstalledSdks {
      name
      categories
      lastSeenDate
    }
  }
`;
