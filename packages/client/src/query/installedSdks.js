import { gql } from "@apollo/client";

export const InstalledSdks = gql`
  mutation InstalledSdks {
    installedSdks {
      name
      categories
      lastSeenDate
    }
  }
`;
