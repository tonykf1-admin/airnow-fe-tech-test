const fs = require('fs');
const { ApolloServer, gql } = require('apollo-server');

const installedSdksRecord = JSON.parse(fs.readFileSync('assets/installed.json', 'utf-8'));
const uninstalledSdksRecord = JSON.parse(fs.readFileSync('assets/uninstalled.json', 'utf-8'));

// The GraphQL schema
const typeDefs = gql`
  type Query {
    installedSdks: [Record],
    uninstalledSdks: [Record]
  }

  type Mutation {
    installedSdks: [Record],
    uninstalledSdks: [Record]
  }

  type Record {
    id: Int,
    name: String,
    categories: [String],
    firstSeenDate: String,
    lastSeenDate: String,
  }
`;

// A map of functions which return data for the schema.
const resolvers = {
  Query: {
    installedSdks: () => installedSdksRecord.data.installedSdks,
    uninstalledSdks: () => uninstalledSdksRecord.data.uninstalledSdks
  },
  Mutation: {
    installedSdks: () => installedSdksRecord.data.installedSdks,
    uninstalledSdks: () => uninstalledSdksRecord.data.uninstalledSdks
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});